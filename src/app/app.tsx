import {
  ConfigurableMapConnect,
  CoreThemeStoreProvider,
  LayerManagerConnect,
  LayerSelectConnect,
  store,
} from '@opengeoweb/core';

export function App() {
  return (
    <div
      style={{
        width: '100%',
        height: '100vh',
      }}
    >
      <CoreThemeStoreProvider store={store}>
				<LayerManagerConnect />
				<LayerSelectConnect />
        <ConfigurableMapConnect layers={[]} />
      </CoreThemeStoreProvider>
    </div>
  );
}

export default App;
